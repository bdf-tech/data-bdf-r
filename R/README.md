##Description
```
package bdf
```
Provides utility functions for centralised packages management, versioning and authentication for the BigDataFed CRAN repository located at https://n2pxhso7m6dqc1is.bigdatafed.com/R/

#First time usage
```
options("download.file.method" = "wget")
options('download.file.extra' = '--header="Authorization: Bearer <token>"')
install.packages("bdf", repos = "https://api.predictionvalley.com/R/", type="source")
```
This will install the latest version of bdf library.
```
devtools::install_bitbucket('bdf-tech/data-bdf-r/R', dependencies=TRUE, type = 'source')
```

##Functions provided for package management
```
bdf::packages_install(pkg)
bdf::packages_update(pkg [optional])
bdf::packages_remove(pkg)
bdf::packages_available(pkg [optional])
bdf::status()
```

Functions are self explanatory, are actual wrappers above the standard package management + automatic contrib utl handling, handy for use but not required.

##Functions provided for authentication
```
bdf::setAuth(token)
```
Saves the authentication token in ~/.bdf_profile

Authentication token is persistent and is loaded every time the getAuth() is called and is used in subsequent calls to other APIs so you better secure that file.


